--TEST--
Check basic functionality of check_plain_ext().
--SKIPIF--
<?php if (!extension_loaded("drupal_extension")) print "skip"; ?>
--FILE--
<?php
  // $Id$

  function check_plain() {}
  function drupal_static() {}

  drupal_extension_enable();
  echo check_plain('test & <toto>');
?>
--EXPECT--
test &amp; &lt;toto&gt;
